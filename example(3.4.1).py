# File name: exampleconvergence-example(3.4.1).py
# Author: Abu bakr Elbukhari
# Date created: April/2016
# Using ultraspherical method to compute approximate solution to the ODE u'(x)+u(x)= 1   , u(+1)=0
# Begin code

from scipy import linalg
from numpy import linalg as LA
from scipy import random, linalg, dot, diag, all, allclose
from numpy.linalg import solve
import numpy as np
import math
import matplotlib.pyplot as plt

n= 50
# Function to generate the differentiation operator matrix S
def diffmat5_v2(n,l):
    "Make differentiation matrix of size 5x5"

    D1 = np.zeros(shape=(n,n))
    for k in range(0,n-l):
            D1[k][k+l] = 2**(l-1) * math.factorial(l-1) * (l+k)
    return D1
l=1                                                          #l is the parameter lambda
D1 = diffmat5_v2(n,l)
print  "D1 = ",D1

# Function to generate the conversion operator matrix S
def convertmat(n):
    "Make differentiation matrix of size nxn"
    S0 = np.zeros(shape=(n,n))

    for j in range(n-2):
        S0[j][j+2] = -0.5
        for j in range(n):
            S0[j][j] = 0.5
            if  S0[1][1] == 0.5:
                S0[0][0]=1
    return S0
S0 = convertmat(n)
print "S0=", S0

# Now we can add the matrices D1 + S0 
A1 = D1 + S0 
print "A1 = ", A1

# Add the boundary conditions
for k in range(0,n):
    A1[n-1,k] = 1;

print "A1 = ", A1

# Maker the righthand side
rhs = np.zeros(shape=(n,1))
rhs[0] = 1
print "rhs=", rhs

# Solve thelinear system
u = solve(A1, rhs)
print "u = ", u

# Function return f(x) by using the chebyshev coefficients u
def clenshaw(u, q):
    bk1 = 0 * q
    bk2 = bk1
    n = len(u)
    for k in range(n-1):
        bk = u[n-1-k] + 2*q*bk1 - bk2
        bk2 = bk1
        bk1 = bk
    y = u[0] + q*bk1 - bk2
    return y
q = np.linspace(-1,1,101)

#x = np.array([-np.cos(np.pi*j/n) for j in range(n+1)])
ux = clenshaw(u, q)
#print "ux=", ux

#plot the solution
x = np.linspace(-1,1,101)
v = 1 - np.exp(1-x)
plt.plot(x, v,'g^',markersize=10,label='Exact')
plt.plot(q, ux,'r-',markersize=10,label='Approximate, N=50')
plt.xlim(-1.0, 1.0)
plt.legend(loc=2)
plt.title(' Exact vs approximated solution plot',fontsize = 17)
plt.xlabel('x values',fontsize = 15)
plt.ylabel(' u(x)',fontsize = 13)
plt.show()

