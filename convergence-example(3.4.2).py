# File name: convergence-example(3.4.2).py
# Author: Abu bakr Elbukhari
# Date created: April/2016

# Begin code

# Computing the converfence of u''(x)+u(x)= 1 ,  u(+1\-1)=0

from scipy import linalg
from numpy import linalg as LA
from scipy import random, linalg, dot, diag, all, allclose
from numpy.linalg import solve
import numpy as np
import math
import matplotlib.pyplot as plt
ln = []
ln1  = []
for n in range(1,20):

    
    # Function to generate the differentiation operator matrix S
    def diffmat5_v2(n,y):
        "Make differentiation matrix of size 5x5"

        D2 = np.zeros(shape=(n,n))
        for k in range(0,n-y):
            D2[k][k+y] = 2**(y-1) * math.factorial(y-1) * (y+k)
        return D2
    y=2                                                          #y is the parameter lambda
    D2 = diffmat5_v2(n,y)
   #print  "D2 = ",D2


    # Function to generate the conversion operator matrix S
    def convertmat0(n):
        "Make differentiation matrix of size nxn"
        S0 = np.zeros(shape=(n,n))

        for j in range(n-2):
            S0[j][j+2] = -0.5
            for j in range(n):
                S0[j][j] = 0.5
                if  S0[1][1] == 0.5:
                    S0[0][0]=1
        return S0
    S0 = convertmat0(n)
    #print "S0=", S0

    # Function to generate the conversion operator matrix S
    def convertmat(n,y):
        "Make converison matrix of size nxn"
        S1 = np.zeros(shape=(n,n))
        for s in range(2,n+1):
            for k in range(1,n-2):
                S1[0][0+2] = -y/float(y+1+1)
                S1[k][k+2] = -y/float(y+k+2)
                #for r in range(0,n-1):
                for k in range(n-1):
                    S1[k][k] = y/float(y+k)
                    S1[k+1][k+1] = y/float(y+k+1)
        return S1
    y=1    
    S1 = convertmat(n,y)
    #print  "S1=",S1

    # Now we can add the matrices (D0 * D1 )+ (S0 * S1)
    A2 = D2 + dot(S1,S0)  
    #print "A2 = ", A2

    # Add the boundary conditions
    for k in range(0,n):
        A2[n-2,k] = 1;
    for k in range(0,n,2):
        A2[n-1,k] = 1;
    for k in range(1,n,2):
        A2[n-1,k] = -1;
    #print "A2 = ", A2

    # Maker the righthand side
    rhs = np.zeros(shape=(n,1))
    rhs[0] = 1
    #print "rhs=", rhs
    # Solve thelinear system
    u = solve(A2, rhs)
   #print "u = ", u

    # Function return f(x) by using the chebyshev coefficients u
    def clenshaw(u, x):
        bk1 = 0 * x
        bk2 = bk1
        n = len(u)
        for k in range(n-1):
            bk = u[n-1-k] + 2*x*bk1 - bk2
            bk2 = bk1
            bk1 = bk
        y = u[0] + x*bk1 - bk2
        return y
    x = np.linspace(-1,1,101)
    ux = clenshaw(u, x)  
    
    # analytic solution
    v = 1 - np.cos(x)/np.cos(1)   
    #for i in range(len(ux)):
        #er = math.log10(v -ux)
    er=(ux-v)
    nm=np.log10(LA.norm(er, 2))
    nm12=[]
    nm12.append(nm)
    op=np.array(nm12)
    ln.append(n)
    ln1.append(float(op))

# Plot the convegence    
plt.plot(ln, ln1,'o-')
#plt.plot(x, ux,'r^-')
#plt.plot(x, v,'+-')
plt.xlabel('N',fontsize = 13)
plt.ylabel('log10$||u_{approix}-u_{exact}||_2$',fontsize = 13)
plt.show()

